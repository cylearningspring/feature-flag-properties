package com.gitlab.chkypros.featureflagproperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeatureFlagPropertiesApplication {

    public static void main(String[] args) {
        SpringApplication.run(FeatureFlagPropertiesApplication.class, args);
    }

}
