package com.gitlab.chkypros.featureflagproperties.featureflag;

import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class PathFeatureFlagMapper implements FeatureFlagMapper {

    @Override
    public String getPrefix() {
        return "path";
    }

    @Override
    public FeatureFlag map(String property) {
        return new PathFeatureFlag(property);
    }

    private static class PathFeatureFlag implements FeatureFlag {
        private final String path;

        public PathFeatureFlag(String path) {
            this.path = path;
        }

        @Override
        public boolean isEnabled() {
            return Files.exists(Paths.get(path));
        }
    }
}
