package com.gitlab.chkypros.featureflagproperties.featureflag;

import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class SpringPropFeatureFlagMapper implements FeatureFlagMapper, EnvironmentAware {

    private Environment environment;

    @Override
    public String getPrefix() {
        return "property";
    }

    @Override
    public FeatureFlag map(String property) {
        return new SpringPropFeatureFlag(property, environment);
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    private static class SpringPropFeatureFlag implements FeatureFlag {
        private final String propertyKey;
        private final Environment environment;

        public SpringPropFeatureFlag(String propertyKey, Environment environment) {
            this.propertyKey = propertyKey;
            this.environment = environment;
        }

        @Override
        public boolean isEnabled() {
            return Boolean.parseBoolean(environment.getProperty(propertyKey));
        }
    }
}
