package com.gitlab.chkypros.featureflagproperties.featureflag;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeatureFlagFactory {

    private static final FeatureFlag ALWAYS_OFF_FEATURE_FLAG = () -> false;

    private final List<FeatureFlagMapper> featureFlagMappers;
    private final Environment environment;

    public FeatureFlagFactory(List<FeatureFlagMapper> featureFlagMappers, Environment environment) {
        this.featureFlagMappers = featureFlagMappers;
        this.environment = environment;
    }

    public FeatureFlag getFlagFor(String mappingProperty) {
        final String propertyMapping = environment.getRequiredProperty(mappingProperty);
        final int indexOfColon = propertyMapping.indexOf(":");
        final String prefix = propertyMapping.substring(0, indexOfColon);
        final String property = propertyMapping.substring(indexOfColon + 1);

        return featureFlagMappers.stream()
                .filter(p -> p.getPrefix().equals(prefix))
                .findFirst()
                .map(p -> p.map(property))
                .orElse(ALWAYS_OFF_FEATURE_FLAG);
    }
}
