package com.gitlab.chkypros.featureflagproperties.featureflag;

public interface FeatureFlag {
    boolean isEnabled();
}
