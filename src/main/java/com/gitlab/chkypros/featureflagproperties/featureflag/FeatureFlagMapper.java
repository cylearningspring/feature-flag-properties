package com.gitlab.chkypros.featureflagproperties.featureflag;

public interface FeatureFlagMapper {
    String getPrefix();

    FeatureFlag map(String property);
}
