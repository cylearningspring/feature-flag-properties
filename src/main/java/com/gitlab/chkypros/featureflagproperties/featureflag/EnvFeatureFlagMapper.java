package com.gitlab.chkypros.featureflagproperties.featureflag;

import org.springframework.stereotype.Service;

@Service
public class EnvFeatureFlagMapper implements FeatureFlagMapper {

    @Override
    public String getPrefix() {
        return "env";
    }

    @Override
    public FeatureFlag map(String property) {
        return new EnvFeatureFlag(property);
    }

    private static class EnvFeatureFlag implements FeatureFlag {
        private final String variable;

        public EnvFeatureFlag(String variable) {
            this.variable = variable;
        }

        @Override
        public boolean isEnabled() {
            return Boolean.parseBoolean(System.getProperty(variable));
        }
    }
}
