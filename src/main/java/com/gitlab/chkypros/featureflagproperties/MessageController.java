package com.gitlab.chkypros.featureflagproperties;

import com.gitlab.chkypros.featureflagproperties.featureflag.FeatureFlag;
import com.gitlab.chkypros.featureflagproperties.featureflag.FeatureFlagFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {
    private final FeatureFlag longMessageFlag;

    public MessageController(
            FeatureFlagFactory flagFactory
    ) {
        this.longMessageFlag = flagFactory.getFlagFor("feature.long.message");
    }

    @GetMapping("/msg")
    public String getMessage() {
        StringBuilder sb = new StringBuilder("This is a ");

        if (longMessageFlag.isEnabled()) {
            sb.append("long ");
        }

        sb.append("message.");
        return sb.toString();
    }
}
